from flask import render_template, flash, request, redirect, session
from flask_mail import Message
from app import app, mail
from .forms import SignUpForm, SignInForm, ChangePasswordForm, ModuleForm, CourseForm, LecturerForm
from app import db, models, bcrypt
from sqlalchemy import Table
import logging, os
from flask_login import LoginManager, login_user,logout_user,current_user,login_required
from flask_admin import Admin

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.session_protection = "strong"

logger = logging.getLogger(__name__)
logging.basicConfig(filename='logging.log', level=logging.INFO)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

@app.route('/', methods=['GET', 'POST'])
def main():
    if current_user.is_authenticated:
        return redirect('/')
    else:
        user = None
        sign_in_form = SignInForm()
        if request.method == 'GET':
            return render_template('/sign_in.html', form=sign_in_form, user = user)

        user = models.Student.query.filter_by(student_email= request.form["student_email"]).first()
        if user is not None and bcrypt.check_password_hash(user.student_password, request.form['student_password']):
            flash("Hello {}".format(user.student_name))
            login_user(user, remember = True)
            logger.info("New user logged in: session[current_user_email] = {}".format(user.student_email))
            return redirect('/display_modules')
        else:
            flash("Invalid email/password!")
            logger.warning("Invalid {} when trying to log in".format(user))
            user = None

        return render_template('/sign_in.html',
                                title='Sign In',
                                user = user,
                                form=sign_in_form)

# Registration page
@app.route('/sign_up', methods=['GET', 'POST'])
def sign_up():
    all_courses = models.Course.query.all()
    if current_user.is_authenticated:
        return redirect('/')
    else:
        user = None
        sign_up_form = SignUpForm()

        #when the sign_up button is clicked
        if sign_up_form.validate_on_submit():
            all_users = models.Student.query.all()
            #check if the user already exists
            for user in all_users:
                if request.form["student_email"] == user.student_email:
                        flash("User already exists!")
                        user = None
                        logger.warning("{} already exists".format(user))
                        return redirect('/sign_up')

            # Check if the email and confirm email fields match
            if request.form["confirm_email"] != request.form["student_email"]:
                flash("The two emails need to be the same!")
                logger.warning("Email and confrim email fields do not match")
                return redirect('/sign_up')
            else:
                #add the user to the db
                user = models.Student(sign_up_form.student_email.data, sign_up_form.student_password.data, sign_up_form.student_name.data, sign_up_form.current_year.data)
                selected_course = models.Course.query.filter_by(course_title = sign_up_form.course_id.data).first()
                user.course = selected_course
                db.session.add(user)

                db.session.commit()
                flash("Registration successful!")
                logger.info("{} added to the database".format(user))
                return redirect('/sign_in')

        return render_template('/sign_up.html',
                                title='Sign Up',
                                user=user,
                                form=sign_up_form,
                                courses=all_courses)


# Login
@app.route('/sign_in', methods=['GET', 'POST'])
def sign_in():

    if current_user.is_authenticated:
        return redirect('/')
    else:
        user = None
        sign_in_form = SignInForm()
        if request.method == 'GET':
            return render_template('/sign_in.html', form=sign_in_form, user = user)

        user = models.Student.query.filter_by(student_email= request.form["student_email"]).first()
        if user is not None and bcrypt.check_password_hash(user.student_password, request.form['student_password']):
            flash("Hello {}".format(user.student_name))
            login_user(user, remember = True)
            logger.info("New user logged in: session[current_user_email] = {}".format(user.student_email))
            return redirect('/display_modules')
        else:
            flash("Invalid email/password!")
            logger.warning("Invalid {} when trying to log in".format(user))
            user = None

        return render_template('/sign_in.html',
                                title='Sign In',
                                user = user,
                                form=sign_in_form)

@login_required
@app.route('/sign_out', methods=['GET', 'POST'])
def sign_out():
    if current_user.is_authenticated:
        user = current_user
        #sign out the current user
        logout_user()
        logger.info ("Sign out: session['current_user_email'] = []")

        flash("Successfully signed out!")

        return redirect('/')

@login_required
@app.route('/create_module', methods=['GET', 'POST'])
def create_module():
    # Only allow an admin to create a module
    if current_user.is_authenticated:
        if current_user.student_email == 'admin@leeds.ac.uk':
            user = current_user
            module_form = ModuleForm()

            #when the sign_up button is clicked
            if module_form.validate_on_submit():
                all_modules = models.Module.query.all()

                if all_modules:
                    for module in all_modules:
                        #check if the user already exists
                        if module_form.module_title.data == module.module_title:
                            flash("Module already exists!")
                            logger.warning("{} already exists".format(module))
                            return redirect('/create_module')

                        else:
                            #add the module to db
                            module = models.Module(module_form.module_title.data, module_form.module_code.data,
                                                   module_form.module_credits.data, module_form.module_year.data,
                                                   module_form.module_semester.data, module_form.class_size.data,
                                                   module_form.module_description.data)
                            teacher = models.Lecturer.query.filter_by(lecturer_name = module_form.lecturer_id.data).first()
                            module.lecturer = teacher
                            db.session.add(module)
                            db.session.commit()
                            flash("Module added successfully!")
                            logger.info("{} added to the database".format(module))
                            #change to display_courses
                            return redirect('/display_modules')
                else:
                    #add the course to db
                    module = models.Module(module_form.module_title.data, module_form.module_code.data,
                                           module_form.module_credits.data, module_form.module_year.data,
                                           module_form.module_semester.data, module_form.class_size.data,
                                           module_form.module_description.data, module_form.prerequisites.data)
                    db.session.add(module)
                    db.session.commit()
                    flash("Module added successfully!")
                    logger.info("{} added to the database".format(module))
                    #change to display_courses
                    return redirect('/display_modules')

            return render_template('/create_module.html',
                                    user = user,
                                    title='Create Module',
                                    form=module_form)
    return redirect('/')

# Display modules
@login_required
@app.route('/display_modules', methods=['GET', 'POST'])
def display_modules():
    user = current_user
    all_modules = models.Module.query.all()

    return render_template('/display_modules.html',
                            title='Display Modules',
                            modules = all_modules,
                            user = user)
                            
# Display modules in module chooser
@login_required
@app.route('/display_modules_in_mch', methods=['GET', 'POST'])
def display_modules_in_mch():
    user = current_user
    all_modules = models.Module.query.all()

    return render_template('/module_chooser.html',
                            title='Display Modules in Module Chooser',
                            modules = all_modules,
                            user = user)

@login_required
@app.route('/display_module_details/<int:id>', methods=['GET', 'POST'])
def display_module_details(id):
    user = current_user
    module= models.Module.query.get(id)
    lecturers = models.Lecturer.query.all()

    all_modules = models.Module.query.all()
    prerequisites = []
    module_prereq_list = []

    # Check if a module has been removed and update prerequisites
    if module.prerequisites and module.prerequisites != "":
        module_prereq_list = module.prerequisites.split("|")
        module.prerequisites = update_prerequisites(module_prereq_list)
        db.session.commit()

    # Make sure the current module doesn't appear on the prerequisites
    for prereq_module in all_modules:
        if prereq_module.module_title != module.module_title:
            unique=True
            for cur_module_prereq in module_prereq_list:
                if prereq_module.module_title == cur_module_prereq:
                    unique = False
            if unique == True:
                prerequisites.append(prereq_module)


    return render_template('/module_details.html',
                            title='Display Module Details',
                            user = user,
                            lecturers=lecturers,
                            module=module,
                            prerequisites=prerequisites,
                            module_prereq_list=module_prereq_list)

# Removes a module from the prerequisities of another module
@login_required
@app.route('/remove_prereq_from_module/<int:module_id>/<string:prereq>', methods=['GET', 'POST'])
def remove_prereq_from_module(module_id, prereq):
    if current_user.is_authenticated:
        if current_user.student_email == "admin@leeds.ac.uk":
            module = models.Module.query.get(module_id)

            prerequisites = module.prerequisites
            prereq_list = prerequisites.split("|")
            for i in range(len(prereq_list)):
                if prereq_list[i] == prereq:
                    del prereq_list[i]
                    break

            prerequisites = "|".join(prereq_list)
            module.prerequisites = prerequisites
            db.session.commit()

            return redirect('/display_module_details/' + str(module_id))

    return redirect ('/')

@login_required
@app.route('/add_lecturer_to_module', methods=['GET', 'POST'])
def add_lecturer_to_module():
    # Get the lecturer and module based on the admin's choice
    lecturer_id = request.form['lecturer_select']
    module_id = request.form['module']

    # Assign the lecturer to the module
    module = models.Module.query.get(module_id)
    module.lecturer_id = lecturer_id

    db.session.commit()
    return redirect('/display_module_details/' + module_id)

# Adds a prerequisite to a course's prerequisities
@login_required
@app.route('/assign_prerequisites_to_module', methods=['GET', 'POST'])
def assign_prerequisites_to_module():
    if current_user.is_authenticated:
        if current_user.student_email == "admin@leeds.ac.uk":
            prereq = request.form['prereq']
            module_id = request.form.get('module')
            module = models.Module.query.get(module_id)
            old_prereq = module.prerequisites

            if prereq == "" or prereq == None or prereq == "--None--":
                old_prereq = str(prereq)
            else:
                old_prereq = old_prereq + "|" + str(prereq)
                module.prerequisites = old_prereq
                db.session.commit()
            return redirect('/display_module_details/' + module_id)

    return redirect('/')


def update_prerequisites(prereq_list):
    all_modules = models.Module.query.all()
    module_titles = []
    for module in all_modules:
        module_titles.append(module.module_title)

    for title in prereq_list:
        if title not in module_titles:
            prereq_list.remove(title)

    new_modules = "|".join(prereq_list)
    return new_modules

@login_required
@app.route('/create_course', methods=['GET', 'POST'])
def create_course():
    # Only allow an admin to create a course
    if current_user.is_authenticated:
        if current_user.student_email == 'admin@leeds.ac.uk':
            user = current_user
            course_form = CourseForm()

            #when the create button is clicked
            if course_form.validate_on_submit():
                all_courses = models.Course.query.all()

                if all_courses:
                    for course in all_courses:
                        #check if the course already exists
                        if course_form.course_title.data == course.course_title:
                            flash("Course already exists!")
                            logger.warning("{} already exists".format(course))
                            return redirect('/create_course')

                        else:
                            #add the course to db
                            course = models.Course(course_form.course_title.data, course_form.course_degree_bach.data,
                                                   course_form.course_degree_mast.data, course_form.course_1y_credits.data,
                                                   course_form.course_2y_credits.data, course_form.course_3y_credits.data,
                                                   course_form.course_4y_credits.data)
                            db.session.add(course)
                            db.session.commit()
                            flash("Course added successfully!")
                            logger.info("{} added to the database".format(course))
                            #change to display_courses
                            return redirect('/display_courses')
                else:
                    #add the course to db
                    course = models.Course(course_form.course_title.data, course_form.course_degree_bach.data,
                                           course_form.course_degree_mast.data, course_form.course_1y_credits.data,
                                           course_form.course_2y_credits.data, course_form.course_3y_credits.data,
                                           course_form.course_4y_credits.data)
                    db.session.add(course)
                    db.session.commit()
                    flash("Course added successfully!")
                    logger.info("{} added to the database".format(course))
                    #change to display_courses
                    return redirect('/display_courses')

            return render_template('/create_course.html',
                                    user = user,
                                    title='Create Course',
                                    form=course_form)
    return redirect('/')

@login_required
@app.route('/display_courses', methods=['GET', 'POST'])
def display_courses():
    user = current_user
    all_courses = models.Course.query.all()

    return render_template('/display_courses.html',
                            title='Display Courses',
                            courses = all_courses,
                            user = user
                            )

@login_required
@app.route('/display_course_details/<int:id>', methods=['GET', 'POST'])
def display_course_details(id):
    user = current_user
    course = models.Course.query.get(id) #post
    all_modules = models.Module.query.all()
    #compulsory_modules = course.compulsory
    compulsory_modules_list = [] #post_terms

    #post = Posts.query.get_or_404(id)
    #post_terms=[]
    for module in course.modules:
        compulsory_modules_list.append(module.id)
    all_modules = models.Module.query.all()
    if request.method == 'POST':
            new_compulsory_module=request.form.getlist('module_id')
            #Add new compulsory module
            for module_id in new_compulsory_module:
                if module_id not in compulsory_modules_list:
                  module=models.Module.query.get(module_id)
                  course.modules.append(module)
            #Remove old post terms which are not included in the update.
            for course_module_id in new_compulsory_module:
                if course_module_id not in new_compulsory_module:
                      module=models.Module.query.get(course_module_id)
                      course.modules.remove(module)

            db.session.commit()

    return render_template('/course_details.html',
                            title='Display Courses Details',
                            user = user,
                            course=course,
                            modules=all_modules,
                            compulsory_modules_list=compulsory_modules_list)

# Remove compulsory module form course
@login_required
@app.route('/remove_compul_from_course/<int:course_id>/<int:compul>', methods=['GET', 'POST'])
def remove_compul_from_course(course_id, compul):
    if current_user.is_authenticated:
        if current_user.student_email == "admin@leeds.ac.uk":
            course = models.Course.query.get(course_id)
            module_d = models.Module.query.get(compul)
            course.modules.remove(module_d)
            db.session.commit()

            return redirect('/display_course_details/' + str(course_id))

    return redirect ('/')


@login_required
@app.route('/create_lecturer', methods=['GET', 'POST'])
def create_lecturer():
    if current_user.is_authenticated:
        if current_user.student_email == 'admin@leeds.ac.uk':
            user = current_user
            lecturer_form = LecturerForm()

            if lecturer_form.validate_on_submit():
                #request the data that is typed in the form
                lecturer = models.Lecturer(lecturer_form.lecturer_name.data, lecturer_form.lecturer_email.data,
                                            lecturer_form.lecturer_degree.data)

                #adds the lecturer to the database
                db.session.add(lecturer)
                db.session.commit()
                flash("Lecturer added successfully!")
                logger.info("{} added to the database".format(lecturer))
                #change to display_trainers
                return redirect('/display_lecturers')

            return render_template('/create_lecturer.html',
                                title='Create Lecturer',
                                user = user,
                                form=lecturer_form)

    return redirect('/')

@login_required
@app.route('/display_lecturers', methods=['GET', 'POST'])
def display_lecturers():
    user = current_user
    #retrieve all trainers from db
    all_lecturers = models.Lecturer.query.all()

    return render_template('/display_lecturers.html',
                            title='Display Lecturers',
                            user = user,
                            lecturers=all_lecturers)

@login_manager.user_loader
def load_user(id):
  return models.Student.query.get(int(id))

@app.before_request
def before_request():
   guest = current_user
