from app import app
from app import db, models
from datetime import datetime, timedelta, date

#Reserve the username admin for the owner who manages the website
admin = models.Student(student_email="admin@leeds.ac.uk", student_password="admin", student_name="admin", current_year = 0)
student_1 = models.Student(student_email="sc15ig@leeds.ac.uk", student_password="1111", student_name="Ivo O. Ganchev", current_year = 1)


lecturer = models.Lecturer("Nick Efford", "nick.efford@leeds.ac.uk", "Dr")
lecturer_1 = models.Lecturer("Karim Djemame", "k.djemame@leeds.ac.uk", "Dr")

module_1 = models.Module("Databases", "COMP1121", 10, 1, 2, 200,
                         "Databases are a common component of many computer systems, storing and retrieving data about the state of a system.")
module_2 = models.Module("Object Oriented Programming", "COMP1721", 10, 1, 2, 200,
                         "Object oriented programming is an industry standard programming technique and is a vital skill for employability.")


course_1 = models.Course(course_title="Computer Science", course_degree_bach="BSc", course_degree_mast="MEng", course_1y_credits=120,
                         course_2y_credits=120, course_3y_credits=120, course_4y_credits=120)
course_2 = models.Course(course_title="Computer Science with Artificial Intelligence", course_degree_bach="BSc", course_degree_mast="MEng", course_1y_credits=120,
                         course_2y_credits=120, course_3y_credits=120, course_4y_credits=120)

record = models.Record(1, student_1.id, module_1.id, 0)
student_1.record.append(record)
module_1.record.append(record)
#course_1.class_size+=1
# compulsory = models.CompulsoryModules(1, course_1.id, module_1.id)
# course_1.compulsory.append(compulsory)
# module_1.compulsory.append(compulsory)


db.session.add(admin)
db.session.add(student_1)
db.session.add(lecturer)
db.session.add(lecturer_1)
db.session.add(module_1)
db.session.add(module_2)
db.session.add(course_1)
db.session.add(course_2)


db.session.commit()
